//
//  EventCell.swift
//  Smurphy
//
//  Created by Dalia Danila on 26/06/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {
    
    @IBOutlet weak var eventName: UILabel!

    @IBOutlet weak var startDate: UILabel!
    
    @IBOutlet weak var endDate: UILabel!
    
    @IBOutlet weak var participantName: UILabel!
    
    @IBOutlet weak var status: UILabel!

}
