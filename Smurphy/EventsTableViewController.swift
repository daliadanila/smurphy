//
//  EventsTableViewController.swift
//  EventTracker
//
//  Created by Dalia Danila on 29/01/2017.
//  Copyright © 2017 Andrew Bancroft. All rights reserved.
//

import UIKit
import EventKit
import Contacts
import MessageUI

class EventsTableViewController: UITableViewController, MFMessageComposeViewControllerDelegate {
    
    var allEvents: [String: NSMutableArray]?
    
    var allSections: [String]?
    
    var currentCalendarName: String?
    
    let eventStore = EKEventStore()
    
    
    let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.allEvents = [String: NSMutableArray]()
        
        self.allSections = [String]()
        
        // Check selected calendar and display events in it.
        
        let defaults = UserDefaults.standard
        
        currentCalendarName = defaults.object(forKey:"SelectedCalendar") as? String
        
        if currentCalendarName != nil {
            
            self.navigationItem.title = currentCalendarName
            
            displayEventsForSelectedCalendar()
        }
        
        // Get default template message
        
        
        
        let templateMessage = defaults.object(forKey:"MessageTemplate") as? String
        
        if templateMessage == nil {
            
            defaults.set("Hi, Can you confirm our meeting on %@? Thanks", forKey: "MessageTemplate")
        
            defaults.synchronize()
        }
        
        self.refreshControl?.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func refresh(sender: AnyObject) {
        
        self.displayEventsForSelectedCalendar()
        
        self.refreshControl?.endRefreshing()
        
    }

    
    func checkCalendarAuthorizationStatus() {
        let status = EKEventStore.authorizationStatus(for: EKEntityType.event)
        
        switch (status) {
        case EKAuthorizationStatus.notDetermined:
            // This happens on first-run
            requestAccessToCalendar()
        case EKAuthorizationStatus.authorized:
            // Things are in line with being able to show the calendars in the table view
            //if currentCalendarName == nil {
                
                displayAlertToSelectCalendar()
           // }
        case EKAuthorizationStatus.restricted, EKAuthorizationStatus.denied:
            // We need to help them give us permission
            displayAlertWithNeededPermission()
        }
    }
    
    func requestAccessToCalendar() {
        eventStore.requestAccess(to: EKEntityType.event, completion: {
            (accessGranted: Bool, error: Error?) in
            
            if accessGranted == true {
                if self.currentCalendarName != nil
                {
                    self.displayEventsForSelectedCalendar()
                }
                else {
                    self.displayAlertToSelectCalendar()
                }
            } else {
                DispatchQueue.main.async(execute: {
                    self.displayAlertWithNeededPermission()
                })
            }
        })
    }
    
    func displayAlertForEventWithNoParticipants() {
       
        let alertController = UIAlertController(title: "Error", message:
            "This event doesn't have any invitees", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func displayAlertWithNeededPermission() {
        
        let alertController = UIAlertController(title: "Calendar Access", message:
            "We need your permission in order to access you calendar data.", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Go to Settings", style: UIAlertActionStyle.default,handler: { action in
            
            self.openSettings()
            
        }))

        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func displayAlertToSelectCalendar() {
        
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Choose Calendar", preferredStyle: .alert)
        
        
        
        for cal in eventStore.calendars(for: EKEntityType.event) {
            
            let calendarAction = UIAlertAction(title: cal.title, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let defaults = UserDefaults.standard
                defaults.set(cal.title, forKey: "SelectedCalendar")
                defaults.synchronize()
                
                self.currentCalendarName = cal.title
                
                self.displayEventsForSelectedCalendar()
                
                self.navigationItem.title = self.currentCalendarName
                
            })
            
            optionMenu.addAction(calendarAction)
        }
        
        //
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
    
            
        })
        
        
        // 4
    
        optionMenu.addAction(cancelAction)
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func displayEventsForSelectedCalendar() {
        
        // Getting calendar with name
        
        var calUid:String = "?"
        for cal in eventStore.calendars(for: EKEntityType.event) {
            if cal.title == self.currentCalendarName { calUid = cal.calendarIdentifier }
        }
        
        let calendarFetched = eventStore.calendar(withIdentifier: calUid)
        
        if calendarFetched == nil {
            
            displayErrorAlert()
            
            return
        }
        
        
        getEventsForCalendar(calendar: calendarFetched!)
        
        self.tableView.reloadData()
        
    }
    
    func displayErrorAlert() {
        
        let alertController = UIAlertController(title: "Can't find any calendar with this name.", message:
            "Do you want to try again?", preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: "Try again", style: UIAlertActionStyle.default,handler: { action in
            
            self.currentCalendarName = nil
            
            self.displayAlertToSelectCalendar()
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func getEventsForCalendar(calendar: EKCalendar) {
        
        
        // Clean previous data
        
        allEvents?.removeAll()
        
        allSections?.removeAll()
        
        let today = NSDate()
        
        let cal = NSCalendar(calendarIdentifier: NSCalendar.Identifier(rawValue: NSGregorianCalendar))
        
        let endDate = cal?.date(byAdding: NSCalendar.Unit.day, value: 7, to: today as Date, options: [])
        
        let predicate = eventStore.predicateForEvents(withStart: today as Date, end: endDate!, calendars: [calendar])
        let existingEvents = eventStore.events(matching: predicate)

        
        let formatter = DateFormatter()
        
        formatter.dateStyle = .full
        
        
        for event in existingEvents {
            
            let startDate = formatter.string(from: event.startDate)
            
            var sectionEvents = (self.allEvents?[startDate])
            
            if sectionEvents == nil {

                sectionEvents = [event]
            }
            else {
                
                sectionEvents?.add(event)
            }
            
            allEvents?[startDate] = sectionEvents
            
            
            if allSections?.contains(startDate) == false {
            
                allSections?.append(startDate)
            }
           
        }

    }


    
    @IBAction func editTemplate(_ sender: Any) {
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Edit message template", message: "Add '%@' to specify date and time for the event.", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            
            let defaults = UserDefaults.standard
            
            let templateMessage = defaults.object(forKey:"MessageTemplate") as? String
            
            textField.text = templateMessage
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            
            let textField = alert?.textFields![0]
            
            print("Template message: \(textField?.text)")
            
            let defaults = UserDefaults.standard
            
            defaults.set(textField?.text, forKey: "MessageTemplate")
            
            defaults.synchronize()
            
        }))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return (allSections?.count)!
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return allSections?[section]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionTitle = allSections?[section]
        
        let datasourceForSection = allEvents?[sectionTitle!]!
        
        return datasourceForSection!.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let event = getEventAtIndexPath(indexPath)
        
        
        
        let cellIdentifier = "eventCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! EventCell
      
        // Configure the cell...
        
        cell.eventName?.text = event.title
        
        cell.status?.text = getInviteeStatus(statusCode: (event.status.rawValue))
        
        
        cell.startDate?.text = "Starts: " + getLongStringDate(event.startDate)
        
        cell.endDate?.text = "Ends: " + getLongStringDate(event.endDate)
        
        return cell
    }
    
    func getLongStringDate(_ date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .short
        
        let dateString = formatter.string(from: date)
        
        return dateString
    }
    
    func getFullStringDate(_ date: Date) -> String {
        
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        formatter.timeStyle = .short
        
        let dateString = formatter.string(from: date)
        
        return dateString
    }
    
    func getInviteeStatus(statusCode: Int) -> String {
        
        switch statusCode {
        case 1:
            return "Pending"
            
        case 2:
            return "Accepted"
            
        case 3:
            return "Declined"
            
        default:
            return "Unknown"
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.requestAuthorizationForAccesingContactsAndSendText(index:indexPath)
    }
    
    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        performSegue(withIdentifier: "showEventDetails", sender: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90;
    }
    
    
    @IBAction func changeSelectedCalendar(_ sender: Any) {
        
        checkCalendarAuthorizationStatus()
    }
    
    func requestAuthorizationForAccesingContactsAndSendText(index: IndexPath) {
        
        // Request authorization
        
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus {
        case .denied, .restricted:
            
            print("Denied")
            
            displayCantAddContactAlert()
            
        case .authorized:
            
            print("Authorized")
            
            sendText(index:index)
            
        case .notDetermined:
            
            print("Not Determined")
            
            promptForAddressBookRequestAccess(index:index)
        }

    }
    
    
    func sendText(index: IndexPath) {
        
        if (MFMessageComposeViewController.canSendText()) {
            
            let event = getEventAtIndexPath(index)
            
            let invitees = event.attendees;
            
            if invitees != nil {
                
                let names = getInviteesNames(invitees: invitees!) as Array<String>
                
                let numbers = getContactNumbers(names:names) as! Array<String>
                
                self.sendTextAtNumber(numbers:numbers, eventDate:(event.startDate), index:index)
            }
            else {
                
                displayAlertForEventWithNoParticipants()
            }
            
        }
            
        else {
            
            displayCantSendTextAlert()
        }
        
    }
    
    func getEventAtIndexPath(_ indexPath: IndexPath) -> EKEvent {
        
        let sectionTitle = allSections?[indexPath.section]
        
        let datasourceForSection = allEvents?[sectionTitle!]!
        
        let event = datasourceForSection?[indexPath.row] as! EKEvent
        
        return event
    }
    
    func sendTextAtNumber(numbers: [String], eventDate: Date, index: IndexPath) {
        
        if MFMessageComposeViewController.canSendText() {
            
            let dateString = getFullStringDate(eventDate)
            
            
            let messageVC = MFMessageComposeViewController()
            
            let defaults = UserDefaults.standard
            
            let templateMessage = defaults.object(forKey:"MessageTemplate") as? String
            
            messageVC.body =  String(format: templateMessage!, dateString as CVarArg)
            
            messageVC.recipients = numbers
            
            messageVC.messageComposeDelegate = self;
            
            messageVC.title = String(index.row);
            
            self.present(messageVC, animated: false, completion: nil)
            
        }
        else {
            displayCantSendTextAlert()
        }
    }
    
    // MFMessage deletgate methods
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case MessageComposeResult.cancelled:
            print("Message was cancelled")
            self.dismiss(animated: true, completion: nil)
            
            
        case MessageComposeResult.failed:
            print("Message failed")
            self.dismiss(animated: true, completion: nil)
        case MessageComposeResult.sent:
            print("Message was sent")
            self.dismiss(animated: true, completion: nil)
            
        
        }
    }
    
    
    func promptForAddressBookRequestAccess(index: IndexPath) {
        var _: Unmanaged<CFError>? = nil
        ABAddressBookRequestAccessWithCompletion(addressBookRef,{success, error in
            if success {
                
                print ("Access granted")
                
                self.sendText(index:index)
            }
            else
            {
                print ("No access")
                
                self.displayCantAddContactAlert()
            }
        })
        
        
    }
    
    func openSettings() {
        let url = NSURL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url! as URL)
    }
    
    func displayCantAddContactAlert() {
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
                                                    style: .default,
                                                    handler: { action in
                                                        self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func displayCantSendTextAlert() {
        let cantAddContactAlert = UIAlertController(title: "Can't send text",
                                                    message: "Your device is either not capable of sending texts or iMeesages is not enabled on your device.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    
    
    func getInviteesNames(invitees: [EKParticipant]) -> Array <String> {
        
        var names = Array<String>()
        
        for invitee in invitees {
            
            if !invitee.isCurrentUser {
            
                names.append(invitee.name!);
            }
        }
        
        return names;
        
    }
    
    func getContactNumbers(names: Array<String>) -> Array <Any> {
        
        var phoneNumbers = Array<Any>()
        
        for name in names {
                
                let contacts = NSMutableArray()
                
                let req = CNContactFetchRequest(keysToFetch: [
                    CNContactFamilyNameKey as CNKeyDescriptor,
                    CNContactGivenNameKey as CNKeyDescriptor,
                    CNContactPhoneNumbersKey as CNKeyDescriptor,
                    CNContactEmailAddressesKey as CNKeyDescriptor
                    ])
                try! CNContactStore().enumerateContacts(with: req) {
                    contact, stop in
                    print(contact)
                    
                    let firstEmailAddress = contact.emailAddresses.first?.value
                    
                    let fullname = contact.givenName + " " + contact.familyName
                    
                    if fullname == name || firstEmailAddress == name as NSString {
                        
                        contacts.add(contact)
                    }
                }
                
//                if (contacts.count > 1) {
//                    
//                    // Presents alert sheet with all contacts found with same name found and lets user choose
//                    
//                    let alertController = UIAlertController(title: "Error", message: String(format: "Found multiple contacts for %@. Please name them differently in your Contacts.", name), preferredStyle: UIAlertControllerStyle.alert)
//                    
//                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler:nil))
//                    
//                    self.present(alertController, animated: true, completion: nil)
//
//                    return []
//                }
            
                if (contacts.count == 0 || (contacts[0] as! CNContact).phoneNumbers.count == 0) {
                    
                    let alertController = UIAlertController(title: "Error", message: String(format: "Please make sure contact %@ has a mobile number attached in Contacts.", name), preferredStyle: UIAlertControllerStyle.alert)
                    
                    alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,handler:nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                    
                    return []
                }
                
                phoneNumbers.append((((contacts[0] as! CNContact).phoneNumbers.first?.value)! as CNPhoneNumber).stringValue)
                
                
            
        }
        
        return phoneNumbers;
        
    }


}
